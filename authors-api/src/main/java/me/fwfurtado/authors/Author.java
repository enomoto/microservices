package me.fwfurtado.authors;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@AllArgsConstructor
public class Author {
    @Setter
    private Long id;
    private final String name;

    public Author(String name) {
        this.name = name;
    }
}
