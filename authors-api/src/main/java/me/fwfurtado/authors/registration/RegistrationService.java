package me.fwfurtado.authors.registration;

import me.fwfurtado.authors.Author;
import me.fwfurtado.authors.registration.RegistrationController.AuthorForm;
import org.springframework.stereotype.Service;

@Service
class RegistrationService {

    private final RegistrationRepository repository;

    RegistrationService(RegistrationRepository repository) {
        this.repository = repository;
    }

    Long registerBy(AuthorForm form) {
        String name = form.getName();

        repository.findByName(name).ifPresent(author -> { throw new IllegalArgumentException("Authors already exist."); });

        Author author = new Author(name);

        repository.save(author);

        return author.getId();
    }
}
