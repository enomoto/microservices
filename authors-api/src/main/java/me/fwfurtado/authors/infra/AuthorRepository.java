package me.fwfurtado.authors.infra;

import static java.util.Objects.isNull;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import me.fwfurtado.authors.Author;
import me.fwfurtado.authors.detail.DetailRepository;
import me.fwfurtado.authors.registration.RegistrationRepository;
import org.springframework.stereotype.Repository;

@Repository
class AuthorRepository implements DetailRepository, RegistrationRepository {
    private static final AtomicLong IDENTITY = new AtomicLong();
    private static final Map<Long, Author> DATABASE = new HashMap<>();

    static {
        DATABASE.computeIfAbsent(IDENTITY.incrementAndGet(), id -> new Author(id, "Fernando"));
    }

    @Override
    public Optional<Author> findById(Long id) {
        return Optional.ofNullable(DATABASE.get(id));
    }

    @Override
    public Optional<Author> findByName(String name) {
        return DATABASE.values().stream().filter(author -> name.equals(author.getName())).findFirst();
    }

    @Override
    public void save(Author author) {
        if (isNull(author.getId())) {
            author.setId(IDENTITY.incrementAndGet());
        }

        DATABASE.put(author.getId(), author);
    }
}
