package me.fwfurtado.authors.detail;

import java.util.Optional;
import me.fwfurtado.authors.Author;

public interface DetailRepository {

    Optional<Author> findById(Long id);

}
