package me.fwfurtado.authors.detail;

import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

import me.fwfurtado.authors.Author;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
class DetailController {

    private final long port;
    private final DetailRepository repository;

    DetailController(@Value("${server.port}") long port, DetailRepository repository) {
        this.port = port;
        this.repository = repository;
    }

    @GetMapping("{id}")
    ResponseEntity<?> detail(@PathVariable Long id) {
        return repository.findById(id).map(this::toResponse).orElseGet(notFound()::build);
    }

    private ResponseEntity<?> toResponse(Author author) {
        return ok().header("LOCAL-SERVER-PORT", Long.toString(port)).body(author);
    }
}
