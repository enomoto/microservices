package me.fwfurtado.books.detail;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import java.util.Optional;
import me.fwfurtado.books.Book;
import me.fwfurtado.books.detail.DetailController.BookView;
import me.fwfurtado.books.infra.AuthorClient;
import me.fwfurtado.books.infra.AuthorClient.AuthorView;
import org.springframework.stereotype.Service;

@Service
class DetailService {

    private final DetailRepository repository;
    private final AuthorClient authorClient;

    DetailService(DetailRepository repository, AuthorClient authorClient) {
        this.repository = repository;
        this.authorClient = authorClient;
    }

    Optional<BookView> showBookDetailOf(Long id) {
        return repository.findById(id).map(this::enhanceAuthors);
    }

    private BookView enhanceAuthors(Book book) {

        AuthorView author = authorClient.findById(book.getAuthorId());

        return new BookView(book.getId(), book.getTitle(), author);
    }
}
