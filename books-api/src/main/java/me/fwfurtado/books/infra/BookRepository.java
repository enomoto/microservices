package me.fwfurtado.books.infra;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import me.fwfurtado.books.Book;
import me.fwfurtado.books.detail.DetailRepository;
import org.springframework.stereotype.Repository;

@Repository
class BookRepository implements DetailRepository {

    private static final Map<Long, Book> DATABASE = new HashMap<>();

    static {
        DATABASE.computeIfAbsent(1L, id -> new Book(id, "Spring Cloud", 1L));
    }

    @Override
    public Optional<Book> findById(Long id) {
        return Optional.ofNullable(DATABASE.get(id));
    }
}
