package me.fwfurtado.books.infra;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import me.fwfurtado.books.infra.AuthorClient.AuthorClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "authors", fallback = AuthorClientFallback.class)
public interface AuthorClient {

    @GetMapping("{id}")
    AuthorView findById(@PathVariable Long id);

    @Getter
    @NoArgsConstructor(access = AccessLevel.PACKAGE)
    @AllArgsConstructor
    class AuthorView {
        private Long id;
        private String name;
    }

    @Component
    class AuthorClientFallback implements AuthorClient {

        @Override
        public AuthorView findById(Long id) {
            return new AuthorView(id, "Unknown");
        }
    }
}
